class form {

    constructor() {
        this.render = new renderBlock();
    }

    main() {
        this.setHtml();
        this.render.render(this.html);
        this.form = this.findForm();
        this.form.addEventListener('submit', ()=>{
            this.setData();
        });
    }

    findForm() {
        return document.querySelector('.book');
    }

    setData() {
        event.preventDefault();
        let data = new FormData(this.form);
        fetch('/api/add',{
            method:'POST',
            body:data
        })  .then(response => response.json())
            .then(result => this.result(result))
    }

    result(result) {
        if (result.success == true) {
            this.form.reset();
            this.render.clearContainer();
            main.init();
        } else {
           alert('email некоректен');
        }
    }

    setHtml() {
        this.html =  "<div class = 'form-container'>" +
                        "<form class='book' method='post'>" +
                            "<input type='text' name='name' placeholder='Введите имя' required>" +
                            "<input type='text' name='email' placeholder='Введите email' required>" +
                            "<textarea name='text' placeholder='Введите сообщение' required></textarea>" +
                            "<input type='submit'>" +
                        "</form>" +
                    "</div>";
    }

}

class list {

    constructor() {
        this.render = new renderBlock();
    }

    main()
    {
        this.getData();
    }

    getData(){
        fetch('/api/list').then(response => response.json())
            .then(result => this.result(result))
    };

    result(result) {
        let html = '';
        if (result.success == true) {
            html = this.getHtmlResult(result.data);
        } else {
            html = this.getError();
        }
        this.render.render(html);
    }

    getError() {
        return "<div class='error-block'>" +
                    "Список сообщений пуст" +
                "</div>";
    }

    getHtmlResult(data){
        let html = '';
        data.forEach(function (elem) {
           html += "<div class='list-elem'>" +
                        "<div class='list-name'>"+ elem.name + "</div>" +
                        "<div class='list-email'>"+ elem.email +"</div>" +
                        "<div class='list-text'>"+ elem.body +"</div>" +
                    "</div>";
        });
        return "<div class='list-block'>" +
                     html +
                "</div>"
    }
}

class renderBlock {

    constructor() {
        this.class = '.book-container';
        this.getContainer();
    }

    clearContainer() {
        this.container.innerHTML = '';
    }

    render(html) {
        this.container.insertAdjacentHTML('beforeend',html);
    }

    getContainer() {
        this.container = document.querySelector(this.class);
    }
}

class main {

     static init() {
        main.loadForm();
        main.loadList();
    }

    static loadList() {
        let mainList = new list();
        mainList.main();
    }

    static loadForm(){
        let mainForm = new form();
        mainForm.main();
    }
}

main.init();
