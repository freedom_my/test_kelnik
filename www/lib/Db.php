<?php

namespace lib;

use PDO;

class Db
{
    protected $db;
    protected $config;
    function __construct()
    {
        $this->config = require $_SERVER['DOCUMENT_ROOT'].'/config/db.php';
        $this->db = new PDO('mysql:host='.$this->config['host'].';charset=utf8;dbname='.$this->config['dbname'],$this->config['user'],$this->config['password']);
    }

    public function getBaseName()
    {
        return $this->config['dbname'];
    }

    public function query($sql, $params = [])
    {
        $stmt = $this->db->prepare($sql);
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue(':'.$key,$value);
            }
        }
        $stmt->execute();
        return $stmt;
    }

    public function row($sql,$params = [])
    {
        $result = $this->query($sql,$params);
        return $result->fetchAll(PDO::FETCH_ASSOC);

    }
    public function column($sql,$params = [])
    {
        $result = $this->query($sql,$params);
        return $result->fetchColumn();
    }

    public function insert($sql,$params = [])
    {
        $stmt = $this->db->prepare($sql);
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue(':'.$key,$value);
            }
        }
        return $stmt->execute();
    }
}