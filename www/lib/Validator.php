<?php

namespace lib;

class Validator
{
    public static function validateEmail($email)
    {
        if (preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", $email)) {
            return true;
        }else{
           return false;
        }
    }
}