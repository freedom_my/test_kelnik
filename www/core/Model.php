<?php

namespace core;

use lib\Db;

abstract class Model
{
    public $db;
    public $dbName;
    public $tableName;
    public function __construct()
    {
        $this->db = new Db();
        $this->dbName = $this->db->getBaseName();
        if (empty($this->checkTable())) {
            $this->createTable();
        }
    }

    public function checkTable()
    {
        return $this->db->row('SHOW TABLES FROM `'.$this->dbName.'` like "'.$this->tableName.'";');
    }
}