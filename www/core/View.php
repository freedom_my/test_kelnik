<?php

namespace core;

class View
{
    private $layout = 'default';

    public function render($view, $result = '')
    {
        $pathView = self::checkView($view);
        ob_start();
        require $pathView;
        $content = ob_get_clean();
        $pathLayout = self::checkLayout($this->layout);
        require $pathLayout;
    }

    public static function errorCode($code)
    {
        http_response_code($code);
        require $_SERVER['DOCUMENT_ROOT'].'/view/errors/'.$code.'.php';
        exit();
    }

    public function changeLayout($layout)
    {
        $this->layout = $layout;
    }

    public static function checkView($view)
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'].'/view/content/'.$view.'.php')) {
            return $_SERVER['DOCUMENT_ROOT'].'/view/content/'.$view.'.php';
        } else {
            self::errorCode(404);
        }
    }

    public static function checkLayout($layout)
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'].'/view/layouts/'.$layout.'.php')) {
            return $_SERVER['DOCUMENT_ROOT'].'/view/layouts/'.$layout.'.php';
        } else {
            self::errorCode(404);
        }
    }
}