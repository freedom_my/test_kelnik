<?php

namespace core;

use core\View;

abstract class Controller
{
    protected $view;
    protected $model;

    public function __construct()
    {
        $this->view = new View();
    }

    public function loadModel($name)
    {
        $path = 'models\\'.ucfirst($name);

        if (class_exists($path)) {
            return new $path;
        } else {
            View::errorCode(404);
        }
    }
}