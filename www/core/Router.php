<?php

namespace core;

class Router
{
    private $controller;
    private $action;

    public function __construct()
    {
        $urlArray = self::parseUrl();
        $this->changeController($urlArray[0]);
        $this->changeAction($urlArray[1]);
    }

    public function run()
    {
        $pathController = $this->checkController();
        $controller = new $pathController();
        $method = $this->checkAction($controller);
        $controller->$method();
    }

    public static function parseUrl()
    {
        $urlArray = explode('/',explode('?',trim($_SERVER['REQUEST_URI'],'/'))[0]);
        if (count($urlArray) != 2) {
            View::errorCode(404);
        } else {
            return $urlArray;
        }
    }

    public function changeController($controller)
    {
        $this->controller = $controller;
    }

    public function changeAction($action)
    {
        $this->action = $action;
    }

    public function checkController()
    {
        $className = '\controllers\\'.$this->controller.'Controller';
        if (class_exists($className)) {
            return $className;
        } else {
            View::errorCode(404);
        }
    }

    public function checkAction($controller)
    {
        $method = $this->action.'Action';
        if (method_exists($controller,$method)){
            return $method;
        } else {
            View::errorCode(404);
        };
    }
}
