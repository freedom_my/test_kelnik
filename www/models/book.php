<?php

namespace models;

use core\Model;

class book extends Model
{
    public $tableName = 'guest_book';

    public function addBook($data)
    {
        $date =  date("Y-m-d H:i:s");
        $data = $this->db->insert('INSERT INTO '.$this->tableName. ' (dtime,name,email,body) VALUES ("'.$date.'","'.$data['name'].'","'.$data['email'].'","'.$data['text'].'")');
        return $data;
    }
    public function listBook()
    {
        $data = $this->db->row('SELECT * FROM '. $this->tableName. ' ORDER BY dtime DESC LIMIT 5');
        return $data;
    }

    public function createTable()
    {
       $check = $this->db->query("CREATE TABLE guest_book (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, dtime DATETIME NOT NULL,name VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',email VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',body TEXT NOT NULL COLLATE 'utf8_unicode_ci',PRIMARY KEY (id),INDEX dtime (dtime)) COLLATE='utf8_unicode_ci' ENGINE=InnoDB;");
    }
}