<?php

namespace controllers;

use core\Controller;

class bookController extends Controller
{
    public function addAction()
    {
        $this->view->render('book');
    }
}