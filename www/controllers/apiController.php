<?php

namespace controllers;

use core\Controller;
use lib\Validator;

class apiController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->view->changeLayout('api');
        $this->model = $this->loadModel('book');
    }

    public function addAction()
    {
        if (!empty($_POST)) {
            if (Validator::validateEmail($_POST['email'])) {
                $result = $this->model->addBook($_POST);
                $result = json_encode(['success' => $result]);
            } else {
                $result = json_encode(['success'=>false]);
            }
            $this->view->render('add', $result);
        }
    }

    public function listAction()
    {
        if (isset($_POST)) {
            $data = $this->model->listBook();
            if (!empty($data)) {
                $result['success'] = true;
                $result['data'] = $data;
            } else {
                $result['success'] = false;
            }
            $result = json_encode($result);
            $this->view->render('list', $result);
        }
    }
}